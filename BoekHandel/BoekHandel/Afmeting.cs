﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoekHandel
{
    public class Afmeting
    {
        /// <summary>
        /// De lengte van een boek of tijdschrift
        /// </summary>
        public int Lengte { get; set; }

        /// <summary>
        /// De Breete van een boek of tijdschrift
        /// </summary>
        public int Breete { get; set; }

        /// <summary>
        /// De Hoogte van een boek of tijdschrift
        /// </summary>
        public int Hoogte { get; set; }

        /// <summary>
        ///  de default constructor
        /// </summary>
        public Afmeting() { }

        /// <summary>
        /// de constructor
        /// </summary>
        /// <param name="_Lengte"></param>
        /// <param name="_Breete"></param>
        /// <param name="_Hoogte"></param>
        public Afmeting(int _Lengte, int _Breete, int _Hoogte)
        {
            this.Lengte = _Lengte;
            this.Breete = _Breete;
            this.Hoogte = _Hoogte;

        }

        /// <summary>
        /// override van toString
        /// </summary>
        /// <returns>
        /// lengte: ... 
        /// Breete: ... 
        /// Hoogte: ...
        /// </returns>
        public override string ToString()
        {
            return "Lengte:" + Lengte + "\n" + "Breete:" + Breete + "\n" + "Hoogte:" + Hoogte;
        }
    }

}
