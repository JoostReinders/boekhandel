﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoekHandel
{
    public class Boek
    {
        public string Naam { get; set; }
        public string Druk { get; set; }

        public string ISBN { get; set; }

        public int MinimumVoorraad { get; set; }

        public int MaximumVoorraad { get; set; }

        public int Vooraad { get; set; }

        public string BestelRegel { get; }


        public Boek(string _Druk, string _ISBN, int _MinimumVoorraad, int _MaximumVoorraad, string _BestelRegel, int _voorraad)
        {
            this.Druk = _Druk;
            this.ISBN = _ISBN;
            this.MinimumVoorraad = _MinimumVoorraad;
            this.MaximumVoorraad = _MaximumVoorraad;
            this.BestelRegel = _BestelRegel;
            this.Vooraad = _voorraad;
        }

        public override string ToString()
        {
            return "druk:" + Druk + "\n" + "ISBN:" + ISBN + "\n" + "Minimumvoorrad:" + MinimumVoorraad + "\n" + "Maximumvoorraad:" + MaximumVoorraad + "\n" + "Bestelregel:" + BestelRegel;
        }
    }
}
