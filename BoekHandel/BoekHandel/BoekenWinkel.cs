﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoekHandel
{
    public class BoekenWinkel
    {
        #region properties
        /// <summary>
        /// Automated properties.
        /// </summary>
        public string ContactInformatie { get; set; }
        public string OpeningsTijden { get; set; }
        public List<string> Voorraad { get; set; }
        public List<string> NietAfgehandeldeBestellingen { get; set; }
        public OrderItems ObjOrderitems { get; set; }
        #endregion

        #region methods
        /// <summary>
        /// default constructor
        /// </summary>
        public BoekenWinkel() { }

        /// <summary>
        /// Makes an actual order.
        /// </summary>
        /// <returns></returns>
        public void GenereerOrder()
        {
            List<string> bestelItems = new List<string>();
            Order order = new Order(new DateTime(2014,12,10), false, bestelItems);
        }

        /// <summary>
        /// the actions needed to sell an actual book
        /// </summary>
        /// <param name="_ISBN"></param>
        /// <param name="aantal"></param>
        /// <returns></returns>
        public string VerkoopBoek(string _ISBN, int aantal)
        {
            
            
            return "deze functie is nog niet werkend.";
        }
        
        /// <summary>
        /// the actions needed to sell an actual book
        /// </summary>
        /// <param name="_ISSN"></param>
        /// <param name="aantal"></param>
        /// <returns></returns>
        public string VerkoopTijdschrift(string _ISSN, int aantal)
        {
            string x = "deze functie is nog niet werkend";


            return x;
        }

        /// <summary>
        /// makes a book from an object
        /// </summary>
        /// <param name="_objBoek"></param>
        public void NieuwBoek(Boek _objBoek, Vooraad _voorraad)
        {
            _voorraad.Add(_objBoek);
        }

        /// <summary>
        /// makes a book with just the info of it.
        /// </summary>
        /// <param name="_Druk"></param>
        /// <param name="_ISBN"></param>
        /// <param name="_MinumumVoorraad"></param>
        /// <param name="_MaximumVoorraad"></param>
        /// <param name="BestekRegel"></param>
        public void NieuwBoek(string _Druk, string _ISBN, int _MinumumVoorraad, int _MaximumVoorraad, string BestelRegel, Vooraad _voorraad)
        {
            _voorraad.Add(new Boek(_Druk, _ISBN, _MinumumVoorraad, _MaximumVoorraad, BestelRegel, 1));
        }

        /// <summary>
        /// makes a magazine from an object
        /// </summary>
        /// <param name="_objBoek"></param>
        public void NieuwTijdschrift(Tijdschrift tijdschrift, Vooraad _voorraad)
        {
            _voorraad.Add(tijdschrift);
        }

        /// <summary>
        /// makes a magazin from it's properties
        /// </summary>
        /// <param name="_Druk"></param>
        /// <param name="_ISSN"></param>
        /// <param name="_AantalTijdschiftenBestellen"></param>
        /// <param name="Besteldag"></param>
        /// <param name="_PublicateiDag"></param>
        public void NieuwTijdschrift(string _Druk, string _ISSN, int _AantalTijdschiftenBestellen, Enum_dag_van_de_week Besteldag, Enum_dag_van_de_week _PublicateiDag , Vooraad _voorraad, int aantal)
        {
            _voorraad.Add(new Tijdschrift(_ISSN, _AantalTijdschiftenBestellen, Besteldag, _PublicateiDag, aantal));
        }

        /// <summary>
        /// deletes a book
        /// </summary>
        /// <param name="_objBoek"></param>
        public void VerwijderBoeken(Boek _objBoek, Vooraad vooraad)
        {
            vooraad.verwijderBoek(_objBoek);
        }

        /// <summary>
        /// deletes a magazine
        /// </summary>
        /// <param name="_objTijdschrift"></param>
        public void verwijderTijdschrift(Tijdschrift _objTijdschrift, Vooraad vooraad)
        {
            vooraad.verwijderTijdschrift(_objTijdschrift);
        }

        /// <summary>
        /// shows all magazines
        /// </summary>
        /// <returns></returns>
        public string ToonAlleTijdschriften(Vooraad vooraad)
        {
            string x = "";
            for (int i = 0; i < vooraad.Tijdschriften.Count; i++)
            {
                x += "" + vooraad.Tijdschriften[i].Naam + "\n";
            }
            return x;
        }

        /// <summary>
        /// shows all books
        /// </summary>
        /// <returns></returns>
        public string ToonAlleBoeken(Vooraad vooraad)
        {
            string x = "";
            for (int i = 0; i < vooraad.Boeken.Count; i++)
            {
                x += "" + vooraad.Boeken[i].Naam + "\n";
            }
            return x;
        }

        /// <summary>
        /// ????????????????????
        /// </summary>
        public void LaatsteBestellingAfhandelen()
        {

        }

        /// <summary>
        /// shows the content of a certain order
        /// </summary>
        /// <param name="_bestelDatum"></param>
        public string BestellingInZien(DateTime _bestelDatum, OrderItems orders)
        {
            for (int i = 0; i < orders.OrderLijst.Count; i++)
            {
                if (orders.OrderLijst[i].OrderDatum == _bestelDatum)
                {
                    string s = "";
                    bool afgehandeld = orders.OrderLijst[i].OrderAfgehandeld;
                    List<string> kaas = orders.OrderLijst[i].OrderList;
                    for (int x = 0; x < kaas.Count; x++)
                    {
                        s += "" + kaas[i] + " \n";
                    }
                    return "afgehandeld: " + afgehandeld + ", \n" + s;
                }
            }
            return "De bestelling is niet gevonden.";
        }
       
        #endregion
    }
}
