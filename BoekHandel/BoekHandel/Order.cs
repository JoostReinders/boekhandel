﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoekHandel
{
    public class Order
    {
        #region properties
        /// <summary>
        /// automated properties
        /// </summary>
        public DateTime OrderDatum { get; set; }
        public bool OrderAfgehandeld { get; set; }
        public List<string> OrderList { get; set; }
        #endregion

        #region methods
        /// <summary>
        /// default constructor
        /// </summary>
        public Order() { }
        /// <summary>
        /// constructor for easily making it generic
        /// </summary>
        /// <param name="_OrderDatum"></param>
        /// <param name="_afgehandeld"></param>
        /// <param name="_OrderList"></param>
        public Order(DateTime _OrderDatum, bool _afgehandeld, List<string> _OrderList)
        {

        }

        /// <summary>
        /// Adds an items to  the list of order items
        /// </summary>
        public void Toevoegen(string _order)
        {
            this.OrderList.Add(_order);
        }

        /// <summary>
        /// removes an item from the list with orders with the specicified parameter
        /// </summary>
        /// <param name="_order"></param>
        public void Verwijderen(string _order)
        {
            this.OrderList.Remove(_order);
        }
        /// <summary>
        /// removes an item from the order list at the specified index
        /// </summary>
        /// <param name="_index"></param>
        public void Verwijderen(int _index)
        {
            this.OrderList.RemoveAt(_index);
        }


        /// <summary>
        /// returns the items in the list
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string x = "";

            for (int i = 0; i < this.OrderList.Count; i++)
            {
                x += this.OrderList[i] + Environment.NewLine;
            }

            return base.ToString();
        }
        #endregion

    }
}
