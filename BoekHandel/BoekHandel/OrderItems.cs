﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoekHandel
{
    public class OrderItems
    {
        #region properties
        public List<Order> OrderLijst { get; set; }

        public string PrintLastOrder {
            get
            {
                return OrderLijst[OrderLijst.Count - 1].ToString();
            }
        }
        #endregion

        #region methods
        /// <summary>
        /// Default constructor
        /// </summary>
        public OrderItems() {

        }

        /// <summary>
        /// Adds an items to  the list of order items
        /// </summary>
        public void Toevoegen(Order _order)
        {
            this.OrderLijst.Add(_order);
        }

        /// <summary>
        /// removes an item from the list with orders with the specicified parameter
        /// </summary>
        /// <param name="_order"></param>
        public void Verwijderen(Order _order)
        {
            this.OrderLijst.Remove(_order);
        }
        /// <summary>
        /// removes an item from the order list at the specified index
        /// </summary>
        /// <param name="_index"></param>
        public void Verwijderen(int _index)
        {
            this.OrderLijst.RemoveAt(_index);
        }

        /// <summary>
        /// prints the actual
        /// </summary>
        /// <param name="_orderDate"></param>
        /// <returns></returns>
        public string PrintOrderOpdatum(DateTime _orderDate)
        {
            int y = getOrderDate(_orderDate);
            if (y == -1)
            {
                return "this item has not been found.";
            }
            return this.OrderLijst[y].ToString(); ;
        }

        /// <summary>
        /// gets ans order by its date.. return the index
        /// </summary>
        /// <param name="_orderDate"></param>
        /// <returns></returns>
        public int getOrderDate(DateTime _orderDate)
        {
            int x = -1;
            for (int i = 0; i < OrderLijst.Count; i++)
            {
                if (OrderLijst[i].OrderDatum == _orderDate)
                {
                    return i;
                }
            }

            return x;
            
        }
        #endregion
    }
}
