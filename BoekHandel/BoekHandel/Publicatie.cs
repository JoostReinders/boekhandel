﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoekHandel
{
    public class Publicatie
    {
        public string Title { get; set; }

        public string Auteur { get; set; }

        public Enum_Taal Taal { get; set; }

        public Afmeting Afmetingen { get; set; }

        public int Gewicht { get; set; }

        public Decimal Prijs { get; set; }

        public Publicatie(string _Title, string _Auteur, Enum_Taal _Taal, Afmeting _Afmetingen, int _Gewicht, Decimal _Prijs)
        {
            this.Title = _Title;
            this.Auteur = _Auteur;
            this.Taal = _Taal;
            this.Afmetingen = _Afmetingen;
            this.Gewicht = _Gewicht;
            this.Prijs = _Prijs;
        }


        public override string ToString()
        {
            return "Title:" + Title + "\n" + "Auteur:" + Auteur + "\n" + "Taal:" + Taal + "\n" + "Afmeting:" + Afmetingen + "\n" + "Gewicht:" + Gewicht + "\n" + "Prijs:" + Prijs;
        }
    }


}
