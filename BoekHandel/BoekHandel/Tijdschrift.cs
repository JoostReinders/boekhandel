﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoekHandel
{
    public class Tijdschrift
    {
        public string Naam { get; set; }
        public string ISSN { get; set; }

        public int AantalTijdschriftenBestellen { get; set; }

        public int Voorraad { get; set; }

        public Enum_dag_van_de_week Besteldag { get; set; }

        public Enum_dag_van_de_week PublicatieDag { get; set; }


        public Tijdschrift(string _ISSN, int AantalTijdschriftenBestellen, Enum_dag_van_de_week _Besteldag, Enum_dag_van_de_week _PublicatieDag, int _voorraad)
        {
            this.ISSN = _ISSN;
            this.AantalTijdschriftenBestellen = AantalTijdschriftenBestellen;
            this.Besteldag = _Besteldag;
            this.PublicatieDag = _PublicatieDag;
            this.Voorraad = _voorraad;
        }

        public override string ToString()
        {
            return "ISSN:" + ISSN + "\n" + "Aantal tijdschriften te Bestellen:" + AantalTijdschriftenBestellen + "\n" + "Besteldag:" + Besteldag + "\n" + "Publicatie dag:" + PublicatieDag;
        }

    }
}
