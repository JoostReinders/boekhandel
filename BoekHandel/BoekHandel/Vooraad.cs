﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoekHandel
{
    public class Vooraad
    {
        public List<Boek> Boeken { get; set; }

        public List<Tijdschrift> Tijdschriften { get; set; }

        /// <summary>
        /// default constructor
        /// </summary>
        public Vooraad() { }

        /// <summary>
        /// adds a book to the storage
        /// </summary>
        /// <param name="_objBoek"></param>
        public void Add(Boek _objBoek)
        {
            if (!Boeken.Contains(_objBoek))
            {
                Boeken.Add(_objBoek);
            }
            else
            {
                for (int i = 0; i < Tijdschriften.Count; i++)
                {
                    if (Boeken[i] == _objBoek)
                    {
                        Boeken[i].Vooraad++;
                    }
                }
            }
        }

        /// <summary>
        /// adds a magazine to the storage
        /// </summary>
        /// <param name="_objTijdschrift"></param>
        public void Add(Tijdschrift _objTijdschrift)
        {
            if (!Tijdschriften.Contains(_objTijdschrift))
            {
                Tijdschriften.Add(_objTijdschrift);
            }
            else
            {
                for (int i = 0; i < Tijdschriften.Count; i++)
                {
                    if (Tijdschriften[i] == _objTijdschrift)
                    {
                        Tijdschriften[i].Voorraad++;
                    }
                }
            }
        }

        /// <summary>
        /// deletes a book from the actual storage
        /// </summary>
        /// <param name="_objBoek"></param>
        public void verwijderBoek(Boek _objBoek)
        {
            for (int i = 0; i < Boeken.Count; i++)
            {
                if (Boeken[i] == _objBoek)
                {
                    if (Boeken[i].Vooraad == 0)
                    {
                        Boeken.Remove(_objBoek);
                        break;
                    }
                    else
                    {
                        Boeken[i].Vooraad--;
                    }
                }
            }
        }
        /// <summary>
        /// verwijderTijdschrift
        /// </summary>
        /// <param name="tijdschrift"></param>
        public void verwijderTijdschrift(Tijdschrift tijdschrift)
        {
            for (int i = 0; i < Boeken.Count; i++)
            {
                if (Tijdschriften[i] == tijdschrift)
                {
                    if (Tijdschriften[i].Voorraad == 0)
                    {
                        Tijdschriften.Remove(tijdschrift);
                        break;
                    }
                    else
                    {
                        Tijdschriften[i].Voorraad--;
                    }
                }
            }
        }
    }
}
